// Test code for Adafruit GPS modules using MTK3329/MTK3339 driver

#include <Adafruit_GPS.h>
#include <SoftwareSerial.h>
#include <String.h>

SoftwareSerial mySerial(7,8);

Adafruit_GPS GPS(&mySerial);

#define GPSECHO  false

boolean usingInterrupt = false;
const char DeviceId[] = "1";
const char *APIKey = "AIzaSyB4NyJ3xdQbHcj2v0C2CHtmgAPxSXYqmn0";
const char colon = ':';
const char dash = '-';
String json;
void setup(){
  
  Serial.begin(9600);
  Serial.println("GPS basic test!");
 
  GPS.begin(9600);
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);   // 1 Hz update rate
  GPS.sendCommand(PGCMD_ANTENNA);
  delay(1000);
  mySerial.flush();
}

// Interrupt is called once a millisecond, looks for any new GPS data, and stores it

uint32_t timer = millis();
void loop()                     // run over and over again
{

 while(!GPS.newNMEAreceived()) { //Loop until you have a good NMEA sentence
        char c=GPS.read();
    }
    GPS.parse(GPS.lastNMEA()); //Parse that last good NMEA sentence
  // if millis() or timer wraps around, we'll just reset it
  if (timer > millis())  timer = millis();

  // approximately every 2 seconds or so, print out the current stats
  if (millis() - timer > 2000) { 
    timer = millis(); // reset the timer
    //if (GPS.fix) {
      if(true){
      //print json here
      //{"DeviceId":1,"Longitude":21.1234,"Latitude":72.1234,"speed":0,"DateStamp":"2015-08-04 13:50:05","APIKey":"AIzaSyB4NyJ3xdQbHcj2v0C2CHtmgAPxSXYqmn0"}
      createJSON();
      Serial.println(json.length());
      Serial.println(json);
    }
  }
}

//{"DeviceId":1,"Longitude":0.000000,"Latitude":0.000000,"speed":0,
//"DateStamp":"2015-8-10","GPSfix":0,"GPS_SignalQuality":13,"Satellites":3,"GSM_signal":10,"APIKey":"AIzaSyB4NyJ3xdQbHcj2v0C2CHtmgAPxSXYqmn0"}
void createJSON(void){
  json = "{\"DeviceId\":";
  json += DeviceId;
  json +=",\"Longitude\":";
  json += String(GPS.longitudeDegrees,6);
  json += ",\"Latitude\":";
  json += String(GPS.latitudeDegrees,6);
  json += ",\"speed\":";
  json += String(GPS.speed,2);
  json += ",\"DateStamp\":\"";
  json += GPS.month;
  json += dash;
  json += GPS.day;
  json += dash;
  json += GPS.year;
  json += " ";
  json += GPS.hour;
  json += colon;
  json += GPS.minute;
  json += colon;
  json += GPS.seconds;
  json += "\",\"GPSfix\":";
  json += GPS.fix;
  json += ",\"FixQuality\":";
  json += GPS.fixquality;
  json += ",\"Satellites\":";
  json += GPS.satellites;
  //add gsm bars
  json += ",\"APIKey\":\"";
  strcpy_P(buffer, (char*)pgm_read_word(&(APIKey))); 
  json += buffer;
  json += "\"}";
}