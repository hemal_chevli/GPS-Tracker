/* TCP connection single connection
int8_t answer;
int onModulePin= 2;
char aux_str[50];
char ip_data[40]="Test string from GPRS shieldrn";
void setup(){
    pinMode(onModulePin, OUTPUT);
    Serial.begin(115200);
    Serial.println("Starting...");
    power_on();
    delay(3000);
    // sets the PIN code
    sendATcommand2("AT+CPIN=****", "OK", "ERROR", 2000);
    delay(3000);
    Serial.println("Connecting to the network...");
    while( sendATcommand2("AT+CREG?", "+CREG: 0,1", "+CREG: 0,5", 1000)== 0 );
}
void loop(){
    // Selects Single-connection mode
    if (sendATcommand2("AT+CIPMUX=0", "OK", "ERROR", 1000) == 1)
    {
        // Waits for status IP INITIAL
        while(sendATcommand2("AT+CIPSTATUS", "INITIAL", "", 500)  == 0 );
        delay(5000);

        // Sets the APN, user name and password
        if (sendATcommand2("AT+CSTT="APN","user_name","password"", "OK",  "ERROR", 30000) == 1)
        {
            // Waits for status IP START
            while(sendATcommand2("AT+CIPSTATUS", "START", "", 500)  == 0 );
            delay(5000);

            // Brings Up Wireless Connection
            if (sendATcommand2("AT+CIICR", "OK", "ERROR", 30000) == 1)
            {
                // Waits for status IP GPRSACT
                while(sendATcommand2("AT+CIPSTATUS", "GPRSACT", "", 500)  == 0 );
                delay(5000);

                // Gets Local IP Address
                if (sendATcommand2("AT+CIFSR", ".", "ERROR", 10000) == 1)
                {
                    // Waits for status IP STATUS
                    while(sendATcommand2("AT+CIPSTATUS", "IP STATUS", "", 500)  == 0 );
                    delay(5000);
                    Serial.println("Openning TCP");

                    // Opens a TCP socket
                    if (sendATcommand2("AT+CIPSTART="TCP","IP_address","port"",
                            "CONNECT OK", "CONNECT FAIL", 30000) == 1)
                    {
                        Serial.println("Connected");

                        // Sends some data to the TCP socket
                        sprintf(aux_str,"AT+CIPSEND=%d", strlen(ip_data));
                        if (sendATcommand2(aux_str, ">", "ERROR", 10000) == 1)
                        {
                            sendATcommand2(ip_data, "SEND OK", "ERROR", 10000);
                        }

                        // Closes the socket
                        sendATcommand2("AT+CIPCLOSE", "CLOSE OK", "ERROR", 10000);
                    }
                    else
                    {
                        Serial.println("Error openning the connection");
                    }
                }
                else
                {
                    Serial.println("Error getting the IP address");
                }
            }
            else
            {
                Serial.println("Error bring up wireless connection");
            }
        }
        else
        {
            Serial.println("Error setting the APN");
        }
    }
    else
    {
        Serial.println("Error setting the single connection");
    }

    sendATcommand2("AT+CIPSHUT", "OK", "ERROR", 10000);
    delay(10000);
}

void power_on(){

    uint8_t answer=0;

    // checks if the module is started
    answer = sendATcommand2("AT", "OK", "OK", 2000);
    if (answer == 0)
    {
        // power on pulse
        digitalWrite(onModulePin,HIGH);
        delay(3000);
        digitalWrite(onModulePin,LOW);

        // waits for an answer from the module
        while(answer == 0){     // Send AT every two seconds and wait for the answer
            answer = sendATcommand2("AT", "OK", "OK", 2000);
        }
    }

}

int8_t sendATcommand2(char* ATcommand, char* expected_answer1,
        char* expected_answer2, unsigned int timeout){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;

    memset(response, '', 100);    // Initialize the string

    delay(100);

    while( Serial.available() > 0) Serial.read();    // Clean the input buffer

    Serial.println(ATcommand);    // Send the AT command

    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
        // if there are data in the UART input buffer, reads it and checks for the asnwer
        if(Serial.available() != 0){
            response[x] = Serial.read();
            x++;
            // check if the desired answer 1  is in the response of the module
            if (strstr(response, expected_answer1) != NULL)
            {
                answer = 1;
            }
            // check if the desired answer 2 is in the response of the module
            else if (strstr(response, expected_answer2) != NULL)
            {
                answer = 2;
            }
        }
    }
    // Waits for the asnwer with time out
    while((answer == 0) && ((millis() - previous) < timeout));

    return answer;
}
*/
#include <SoftwareSerial.h>
 
SoftwareSerial GPRS_MODEM(5,6);

char *sendmsg[] = {
"AT+CIPSTART=\"TCP\",\"68.232.44.251\",\"80\"",
"AT+CIPSEND",
"POST http://www.w3schools.com/webservices/tempconvert.asmx/CelsiusToFahrenheit HTTP/1.1",
"Host: www.w3schools.com",
"Content-Type: application/x-www-form-urlencoded",
"Content-Length: 10\r\n",
"Celsius=32"
};
unsigned char ctrl_z = 26; 
void setup()
{
  GPRS_MODEM.begin(9600);
 
  Serial.begin(9600);    
  
  delay(5000);
  Serial.println("Starting to sendmsg to Modem now");
}
 
void loop()
{
  int i;
  char rx_byte = 0;
  char rx_prev = 0;
  int notdone = 1;
  
  //Serial.println(sendmsg[0]);
  notdone = 1;
  GPRS_MODEM.println(sendmsg[0]);
  while(notdone)  
  while(GPRS_MODEM.available())
  {
    rx_prev = rx_byte;
    rx_byte = GPRS_MODEM.read();
    Serial.print(rx_byte);
    if(rx_prev != 'O' && rx_byte != 'K')
      notdone = 0;
  }
  notdone = 1;
  while(notdone)  
  while(GPRS_MODEM.available())
  {
    rx_prev = rx_byte;
    rx_byte = GPRS_MODEM.read();
    Serial.print(rx_byte);
    if(rx_prev != 'O' && rx_byte != 'K')
      notdone = 0;
  }
 
  GPRS_MODEM.println(sendmsg[1]);
  delay(200);
 
  for(i=2;i<5;i++)
  {
    GPRS_MODEM.println(sendmsg[i]);
    //Serial.println(sendmsg[i]);
    while(GPRS_MODEM.available())
    {
      rx_byte = GPRS_MODEM.read();
      Serial.print(rx_byte);
    }
    delay(100);
  }
  delay(100);
  GPRS_MODEM.write(ctrl_z);
  
  while(1)
  {
    while(GPRS_MODEM.available())
    {
      rx_byte = GPRS_MODEM.read();
      Serial.print(rx_byte);
    }
    while(Serial.available())
    {
      rx_byte = Serial.read();
      GPRS_MODEM.print(rx_byte);
    }
    //if(rx_byte == 0x0D)
    //  break;
  }
  
}

/*TCP multiple connection
int8_t answer;
int onModulePin= 2;
char aux_str[50];

char ip_data[40]="Test string from GPRS shieldrn";

void setup(){

    pinMode(onModulePin, OUTPUT);
    Serial.begin(115200);

    Serial.println("Starting...");
    power_on();

    delay(3000);

    // sets the PIN code
    sendATcommand2("AT+CPIN=****", "OK", "ERROR", 2000);

    delay(3000);

    Serial.println("Connecting to the network...");

    while( sendATcommand2("AT+CREG?", "+CREG: 0,1", "+CREG: 0,5", 1000) == 0 );

}


void loop(){


    // Selects Multi-connection mode
    if (sendATcommand2("AT+CIPMUX=1", "OK", "ERROR", 1000) == 1)
    {
        // Waits for status IP INITIAL
        while(sendATcommand2("AT+CIPSTATUS", "INITIAL", "", 500)  == 0 );
        delay(5000);

        // Sets the APN, user name and password
        if (sendATcommand2("AT+CSTT="APN","user_name","password"", "OK",  "ERROR", 30000) == 1)
        {
            // Waits for status IP START
            while(sendATcommand2("AT+CIPSTATUS", "START", "", 500)  == 0 );
            delay(5000);

            // Brings Up Wireless Connection
            if (sendATcommand2("AT+CIICR", "OK", "ERROR", 30000) == 1)
            {
                // Waits for status IP GPRSACT
                while(sendATcommand2("AT+CIPSTATUS", "GPRSACT", "", 500)  == 0 );
                delay(5000);

                // Gets Local IP Address
                if (sendATcommand2("AT+CIFSR", ".", "ERROR", 10000) == 1)
                {
                    // Waits for status IP STATUS
                    while(sendATcommand2("AT+CIPSTATUS", "IP STATUS", "", 500)  == 0 );
                    delay(5000);
                    Serial.println("Openning TCP");

                    // Opens a TCP socket with connection 1
                    if (sendATcommand2("AT+CIPSTART=1,"TCP","IP_address","port"",
                                    "CONNECT OK", "CONNECT FAIL", 30000) == 1)
                    {
                        Serial.println("Connected");

                        // Sends some data to the TCP socket
                        sprintf(aux_str,"AT+CIPSEND=1,%d", strlen(ip_data));
                        if (sendATcommand2(aux_str, ">", "ERROR", 10000) == 1)
                        {
                            delay(500);
                            sendATcommand2(ip_data, "SEND OK", "ERROR", 10000);
                        }

                        // Closes the socket
                        sendATcommand2("AT+CIPCLOSE=1", "CLOSE OK", "ERROR", 10000);
                    }
                    else
                    {
                        Serial.println("Error openning the connection 1");
                    }

                }
                else
                {
                    Serial.println("Error getting the IP address");
                }
            }
            else
            {
                Serial.println("Error bring up wireless connection");
            }
        }
        else
        {
            Serial.println("Error setting the APN");
        }
    }
    else
    {
        Serial.println("Error setting the multi-connection");
    }

    sendATcommand2("AT+CIPSHUT", "OK", "ERROR", 10000);
    delay(10000);
}

void power_on(){

    uint8_t answer=0;

    // checks if the module is started
    answer = sendATcommand2("AT", "OK", "OK", 2000);
    if (answer == 0)
    {
        // power on pulse
        digitalWrite(onModulePin,HIGH);
        delay(3000);
        digitalWrite(onModulePin,LOW);

        // waits for an answer from the module
        while(answer == 0){     // Send AT every two seconds and wait for the answer
            answer = sendATcommand2("AT", "OK", "OK", 2000);
        }
    }

}

int8_t sendATcommand2(char* ATcommand, char* expected_answer1,
        char* expected_answer2, unsigned int timeout){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;

    memset(response, '', 100);    // Initialize the string

    delay(100);

    while( Serial.available() > 0) Serial.read();    // Clean the input buffer

    Serial.println(ATcommand);    // Send the AT command

    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
        // if there are data in the UART input buffer, reads it and checks for the asnwer
        if(Serial.available() != 0){
            response[x] = Serial.read();
            x++;
            // check if the desired answer 1  is in the response of the module
            if (strstr(response, expected_answer1) != NULL)
            {
                answer = 1;
            }
            // check if the desired answer 2 is in the response of the module
            else if (strstr(response, expected_answer2) != NULL)
            {
                answer = 2;
            }
        }
    }
    // Waits for the asnwer with time out
    while((answer == 0) && ((millis() - previous) < timeout));

    return answer;
}

*/