#include <SoftwareSerial.h> //Load the Software Serial library
#include <avr/pgmspace.h>   //store data in flash

#define debugging true

SoftwareSerial debug(2,3);//rx,tx
int black=0;
void setup() {
  if (debugging){
    debug.begin(9600);
    debug.flush();
    debug.print("");
      debug.println("Sending AT and AT+CREG?");
  }
  Serial.begin(9600); //Turn on serial monitor
  //send AT to sync bauds
  sendATcommand("AT", "OK", 1000); 
    while( (sendATcommand("AT+CREG?", "+CREG: 0,1", 1000) || sendATcommand("AT+CREG?", "+CREG: 0,5", 1000)) == 0 );//check if gsm is registered on the network
    sendATcommand("AT+CMEE=1", "OK", 1000); //enable error reporting

    if (debugging){
      debug.listen();
      debug.println("Going in loop");    
    }  
}
void loop() {
  //check registration
    sendATcommand("AT+CIPSHUT", "OK", 1000); //reset ip connection if any are open
    sendATcommand("AT+CIPMUX=0", "OK", 1000); //single connection
    //insert if here
    sendATcommand("AT+CGATT?", "OK", 1000); //check if connected +CGATT=1 means connected
    sendATcommand("AT+CGATT=1", "OK", 1000); //attact to GPRS 
    sendATcommand("AT+CSTT=\"TATA.DOCOMO.INTERNET\",\"\",\"\"","OK",1000); //connection data
    sendATcommand("AT+CIICR","OK",5000);
     //bring up connection
    sendATcommand("AT+CIFSR","OK",1000); //returns ip of the device


    if(black==1){
    sendATcommand("AT+CIPSTART=\"TCP\",\"50.87.144.195\",\"80\"","CONNECT",5000); //open connection to server
    //CHECK CONNECTION WITH CIPSTATUS
    //loop here till connection dies out
    sendATcommand("AT+CIPSTATUS","STATE",1000);
    sendATcommand("AT+CIPSEND",">",2000);
    Serial.println();
    delay(100);
    Serial.println("POST /sandbox/posttest.php HTTP/1.1"); //post 
    delay(100);
    Serial.println("Host: black-electronics.com"); //host
    delay(100);
    Serial.println("Content-Type: application/x-www-form-urlencoded"); //content type
    delay(100);
    Serial.println("Content-Length: 15"); //content length
    delay(100);
    Serial.println(); //enter
    Serial.print("var_a=1&var_b=2");
    Serial.write(0x1A);//ctrl-z
    delay(100);
    debug.println("post end-----------------------");
    delay(1000);
    //listen for reply
    while(Serial.available() != 0){    // if there are data in the UART input buffer, reads it and checks for the asnwer
        delay(50);
      char c = Serial.read();
      if(debugging){
          debug.print(c);
      }
    }
    sendATcommand("AT+CIPCLOSE","OK",1000);
    sendATcommand("AT+CIPSTATUS","OK",1000);
  }
  else if (black == 0){
   sendATcommand("AT+CIPSTART=\"TCP\",\"104.43.8.241\",\"8084\"","CONNECT",5000); //open connection to server
    //CHECK CONNECTION WITH CIPSTATUS
    //loop here till connection dies out
    sendATcommand("AT+CIPSTATUS","STATE",1000);
    sendATcommand("AT+CIPSEND",">",2000);
    Serial.println();
    delay(100);
    Serial.println("POST //Webservices.asmx/InsertTrackingData HTTP/1.1"); //post 
    delay(100);
    Serial.println("Host: krtyabackoffice.cloudapp.net:8084"); //host
    delay(100);
    Serial.println("Content-Type: application/json"); //content type
    delay(100);
    Serial.println("Content-Length: 135"); //content length
    delay(100);
    Serial.println(); //enter
    Serial.print("{\"DeviceId\":1,\"Longitude\":\"DE\",\"Latitude\":\"72.1\",\"speed\":\"0\",\"DateStamp\":\"2015-8-5\",\"APIKey\":\"AIzaSyB4NyJ3xdQbHcj2v0C2CHtmgAPxSXYqmn0\"}");
    Serial.write(0x1A);//ctrl-z
    delay(100);
    debug.println("post end-----------------------");
    delay(1000);
    //listen for reply
    while(Serial.available() != 0){    // if there are data in the UART input buffer, reads it and checks for the asnwer
        delay(50);
      char c = Serial.read();
      if(debugging){
          debug.print(c);
      }
    }
    sendATcommand("AT+CIPCLOSE","OK",1000);
    sendATcommand("AT+CIPSTATUS","OK",1000);
  }
}//loop end
//Functions

void readGPS() {

 
}

void send_HTTP(){

}


//--------------------------------------------------
//Functions
int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int timeout){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;

    memset(response, '\0', 100);    // Initialice the string
    
    delay(100);
    
    while( Serial.available() > 0) Serial.read();    // Clean the input buffer
    
    if (ATcommand[0] != '\0')
    {
        Serial.println(ATcommand);    // Send the AT command 
    }


    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
        if(Serial.available() != 0){    // if there are data in the UART input buffer, reads it and checks for the asnwer
            response[x] = Serial.read();
            if(debugging){
                debug.print(response[x]);
            }
            x++;
            if (strstr(response, expected_answer) != NULL)    // check if the desired answer (OK) is in the response of the module
            {
                answer = 1;
            }
        }
    }while((answer == 0) && ((millis() - previous) < timeout));    // Waits for the asnwer with time out

    return answer;
}

/* POST data example
#include <SoftwareSerial.h>
#include <String.h>
SoftwareSerial mySerial(5,6);                                                      //your pins to serial communication
int valor;
//-------------------------------------------------------------
//---------------------Ubidots Configuration-------------------
//-------------------------------------------------------------
String token = "tShIxUgpfmyWpsz0ZKtFdLDxiPmubDqBuGcI8NzlAuN5GK8ynfd0XDpRZH0R";      //your token to post value
String idvariable = "53baaf3c76254244e1c8e408";                                     //ID of your variable
void setup()
{

  mySerial.begin(19200);                                                            //the GPRS baud rate
  Serial.begin(19200);                                                              //the serial communication baud rate
  delay(10000);
}

void loop()
{
    int value = analogRead(A0);                                                     //read pin A0 from your arduino
    save_value(String(value));                                                      //call the save_value function
    if (mySerial.available())
    Serial.write(mySerial.read());
}
//this function is to send the sensor data to Ubidots, you should see the new value in Ubidots after executing this function
void save_value(String value)
{
  int num;
  String le;
  String var;
  var="{\"value\":"+ value + "}";
  num=var.length();
  le=String(num);
  for(int i = 0;i<7;i++)
  {
    mySerial.println("AT+CGATT?");                                                   //this is made repeatedly because it is unstable
    delay(2000);
    ShowSerialData();
  }
  mySerial.println("AT+CSTT=\"web.vmc.net.co\"");                                    //replace with your providers' APN
  delay(1000);
  ShowSerialData();
  mySerial.println("AT+CIICR");                                                      //bring up wireless connection
  delay(3000);
  ShowSerialData();
  mySerial.println("AT+CIFSR");                                                      //get local IP adress
  delay(2000);
  ShowSerialData();
  mySerial.println("AT+CIPSPRT=0");
  delay(3000);
  ShowSerialData();
  mySerial.println("AT+CIPSTART=\"tcp\",\"things.ubidots.com\",\"80\"");             //start up the connection
  delay(3000);
  ShowSerialData();
  mySerial.println("AT+CIPSEND");                                                    //begin send data to remote server
  delay(3000);
  ShowSerialData();
  mySerial.print("POST /api/v1.6/variables/"+idvariable);
  delay(100);
  ShowSerialData();
  mySerial.println("/values HTTP/1.1");
  delay(100);
  ShowSerialData();
  mySerial.println("Content-Type: application/json");
  delay(100);
  ShowSerialData();
  mySerial.println("Content-Length: "+le);
  delay(100);
  ShowSerialData();
  mySerial.print("X-Auth-Token: ");
  delay(100);
  ShowSerialData();
  mySerial.println(token);
  delay(100);
  ShowSerialData();
  mySerial.println("Host: things.ubidots.com");
  delay(100);
  ShowSerialData();
  mySerial.println();
  delay(100);
  ShowSerialData();
  mySerial.println(var);
  delay(100);
  ShowSerialData();
  mySerial.println();
  delay(100);
  ShowSerialData();
  mySerial.println((char)26);
  delay(7000);
  mySerial.println();
  ShowSerialData();
  mySerial.println("AT+CIPCLOSE");                                                //close the communication
  delay(1000);
  ShowSerialData();
}

void ShowSerialData()
{
  while(mySerial.available()!=0)
  Serial.write(mySerial.read());
}
////////////////////////////////////////////////////////////////////////////



#include <SoftwareSerial.h>
SoftwareSerial gprsSerial(7, 8);

void setup()
{
  gprsSerial.begin(19200);
  Serial.begin(19200);

  Serial.println("Config SIM900...");
  delay(2000);
  Serial.println("Done!...");
  gprsSerial.flush();
  Serial.flush();

  // attach or detach from GPRS service 
  gprsSerial.println("AT+CGATT?");
  delay(100);
  toSerial();


  // bearer settings
  gprsSerial.println("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"");
  delay(2000);
  toSerial();

  // bearer settings
  gprsSerial.println("AT+SAPBR=3,1,\"APN\",\"epc.tmobile.com\"");
  delay(2000);
  toSerial();

  // bearer settings
  gprsSerial.println("AT+SAPBR=1,1");
  delay(2000);
  toSerial();
}


void loop()
{
   // initialize http service
   gprsSerial.println("AT+HTTPINIT");
   delay(2000); 
   toSerial();

   // set http param value
   gprsSerial.println("AT+HTTPPARA=\"URL\",\"http://YOUR.DOMAIN.COM/rest/receiveSensorData?sensorval1=blah&sensorval2=blah\"");
   delay(2000);
   toSerial();

   // set http action type 0 = GET, 1 = POST, 2 = HEAD
   gprsSerial.println("AT+HTTPACTION=0");
   delay(6000);
   toSerial();

   // read server response
   gprsSerial.println("AT+HTTPREAD"); 
   delay(1000);
   toSerial();

   gprsSerial.println("");
   gprsSerial.println("AT+HTTPTERM");
   toSerial();
   delay(300);

   gprsSerial.println("");
   delay(10000);
}

void toSerial()
{
  while(gprsSerial.available()!=0)
  {
    Serial.write(gprsSerial.read());
  }
}




*/