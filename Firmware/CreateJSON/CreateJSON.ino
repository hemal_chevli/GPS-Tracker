//Date: 20 July 2015
//arduino 1.0.5
/*
**Description:
Track buses using GPS/GSM tracker
Current hardware:
SIM900
MT3339
atmega328
---------
debugging: 2,3 rx tx 
GSM/SIM900: hardware serial
GPS: 7,8 rx tx 
Status LEDs
Power LED: from supply
On battery LED: from supply
Network led D6
GPS fix status D5

*TODO
Power out detection: A0
SIM900 modem reset pin
SIM900 modem  pwrkey pin
Power supply circuit fuse battery power off detection
Sleep mode for AVR,GSM and GPS
Watchdog timer
Do configuration vis sending sms to sim in the device(load store eeprom)
Buffer GSP commands
check bars
*/

#include <Adafruit_GPS.h>   //Install the adafruit GPS library
#include <SoftwareSerial.h> //Load the Software Serial library
#include <avr/pgmspace.h>

#define debugging true

SoftwareSerial mySerial(7,8); //Initialize the Software Serial port
Adafruit_GPS GPS(&mySerial); //Create the GPS Object
SoftwareSerial debug(2,3);//rx,tx

const char DeviceId[] = "1";
const char colon = ':';
const char dash = '-';

const char POST[] PROGMEM =           "POST //Webservices.asmx/InsertTrackingData HTTP/1.1";
const char HOST[] PROGMEM =           "Host: krtyabackoffice.cloudapp.net:8084";
const char contentType[] PROGMEM =    "Content-Type: application/json";
const char contentLength[] PROGMEM =  "Content-Length: ";
const char* const POSTMethod[] PROGMEM = {POST, HOST, contentType, contentLength};
const char at[]=                    "AT";
const char ipr[]=                   "AT+IPR=9600";
const char creg[] PROGMEM =         "AT+CREG?";
const char cmee[] PROGMEM =         "AT+CMEE=1";
const char cipstatus[] PROGMEM =    "AT+CIPSTATUS";
const char cipshut[] PROGMEM =      "AT+CIPSHUT";
const char cipmux[] PROGMEM =       "AT+CIPMUX=0";
const char isCGATT[] PROGMEM =      "AT+CGATT?";
const char cgatt[] PROGMEM =        "AT+CGATT=1";
const char cstt[] PROGMEM =         "AT+CSTT=\"TATA.DOCOMO.INTERNET\",\"\",\"\"";
const char ciicr[] PROGMEM =        "AT+CIICR";
const char cifsr[] PROGMEM =        "AT+CIFSR";
const char cipstart[] PROGMEM =     "AT+CIPSTART=\"TCP\",\"104.43.8.241\",\"8084\"";
const char cipsend[] PROGMEM =      "AT+CIPSEND";
const char cipclose[] PROGMEM =     "AT+CIPCLOSE";
const char apiKey[] PROGMEM=        "AIzaSyB4NyJ3xdQbHcj2v0C2CHtmgAPxSXYqmn0";

const char* const IPR PROGMEM = ipr;
const char* const CREG PROGMEM = creg;
const char* const CMEE PROGMEM = cmee;
const char* const CIPSTATUS PROGMEM = cipstatus;
const char* const CIPSHUT PROGMEM = cipshut;
const char* const CIPMUX PROGMEM = cipmux;
const char* const ISCGATT PROGMEM = isCGATT;
const char* const CGATT PROGMEM = cgatt;
const char* const CSTT PROGMEM = cstt;
const char* const CIICR PROGMEM = ciicr;
const char* const CIFSR PROGMEM = cifsr;
const char* const CIPSTART PROGMEM = cipstart;
const char* const CIPSEND PROGMEM = cipsend;
const char* const CIPCLOSE PROGMEM = cipclose;
const char* const APIKey PROGMEM = apiKey;
const char* const AT PROGMEM = at;
const int PROGMEM delayBetweenCmds = 200;
//USAGE
//strcpy_P(buffer, (char*)pgm_read_word(&(CREG))); 
//sendATcommand(buffer,"O",1000);

String json;
char buffer[60];
char c; //to read characters coming from the GPS
int connection;
int i;



//Fucntion declarations
void readGPS(void);
void sendData(void);
void connectWebServer(void);
void reconnect(void);
void createJSON(void);
int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int timeout);
int8_t connStatus(void);

void setup() {
  if (debugging){
    debug.begin(9600);
    debug.flush();
    debug.print("");
    debug.println(F("\r\n------------------"));
  }
  //Show info
  //Device ID
  debug.print(F("Device ID:"));
  debug.println(DeviceId);

  debug.print(F("AP:"));
  strcpy_P(buffer, (char*)pgm_read_word(&(CSTT))); 
  debug.println(buffer);

  debug.print(F("Link:"));
  strcpy_P(buffer, (char*)pgm_read_word(&(CIPSTART))); 
  debug.println(buffer);
  debug.println(F("------------------"));

  Serial.begin(9600); //Turn on serial monitor for GSM modem
  for(i=0;i<4;i++){
       Serial.print("A"); //sync baud
       delay(100);
  }
  //delay(4000);
  Serial.println("AT");
  delay(1000);
  Serial.println("AT+IPR=9600");

  //0,1 REGISTERED HOME NW, 0,5 REGISTERED ROAMING
  //strcpy_P(buffer, (char*)pgm_read_word(&(CREG))); 
  ///while( (sendATcommand(buffer, "+CREG: 0,1", 2000) || sendATcommand(buffer, "+CREG: 0,5", 2000)) == 0 );//check if gsm is registered on the network
  delay(delayBetweenCmds);

  strcpy_P(buffer, (char*)pgm_read_word(&(CMEE))); 
  sendATcommand(buffer, "OK", 1000); //enable error reporting

  GPS.begin(9600); //Turn on GPS at 9600 baud
  mySerial.flush();
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA); //Request RMC Sentences only
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ); //Set update rate 1 seconds
  //GPS.sendCommand(PGCMD_ANTENNA);
  delay(1000);
  if (debugging){
    debug.listen();
    debug.println(F("\r\nGoing in loop"));
  }

}//Setup end

void loop() {
  //get bars
  //AT+CSQ
 
  Serial.println("AT+CSQ");
  while(Serial.available()>0){
    responses[a] = Serial.read();
    a++;
    strtok(responses, " ");
    strcpy(signalQ,strtok(NULL, ","));
    debug.print("----------");
    debug.println(signalQ);
    debug.println(a);
  }
  a=0;
  delay(2000);
}//loop end
//--------------------------------------------------
//Functions
int8_t connStatus(){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    memset(response, '\0', 100);    // Initialize the string
    delay(100);
    while( Serial.available() > 0) Serial.read();    // Clean the input buffer
    Serial.flush();
    strcpy_P(buffer, (char*)pgm_read_word(&(CIPSTATUS))); 
    Serial.println(buffer); //send cipstatus command

    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
        if(Serial.available() != 0){    // if there are data in the UART input buffer, reads it and checks for the asnwer
            response[x] = Serial.read();
            if(debugging){
                debug.print(response[x]);
            }
            x++;
            if (strstr(response, "IP INITIAL") != NULL)    
            {
                answer = 1;
            }
            else if(strstr(response, "IP GPRSACT") != NULL)  
            {
                answer = 2;
            }
            else if(strstr(response, "IP STATUS") != NULL)  
            {
                answer = 3;
            }
            else if(strstr(response, "TCP CONNECTING") != NULL)  
            {
                answer = 4;
            }
            else if(strstr(response, "CONNECT OK") != NULL)  
            {
                answer = 5;
            }
            else if(strstr(response, "TCP CLOSED") != NULL)  
            {
                answer = 6;
            }
            else if(strstr(response, "PDP DEACT") != NULL)  
            {
                answer = 7;
            }
            else if(strstr(response, "IP START") != NULL)  
            {
                answer = 8;
            }
        }
    }while((answer == 0) && ((millis() - previous) < 1000));    // Waits for the asnwer with time out

    return answer;
}
void readGPS(void){
    mySerial.listen();
    while(!GPS.newNMEAreceived()) { //Loop until you have a good NMEA sentence
        c=GPS.read();
    }
    GPS.parse(GPS.lastNMEA()); //Parse that last good NMEA sentence
}
void sendData(void){
    unsigned long previous;
    unsigned int waitTime;
    createJSON();
    

    strcpy_P(buffer, (char*)pgm_read_word(&(CIPSEND))); 
    sendATcommand(buffer,">",2000);  //send post request
    Serial.println();
    for (i = 0; i <=2 ; i++){
      strcpy_P(buffer, (char*)pgm_read_word(&(POSTMethod[i]))); 
      Serial.println(buffer);
      delay(50);
    }
    strcpy_P(buffer, (char*)pgm_read_word(&(POSTMethod[3]))); 
    Serial.print(buffer);Serial.println(json.length());
    delay(50);
    Serial.println(); //enter
    Serial.print(json); 
    Serial.write(0x1A);//ctrl-z
    debug.print("JSON SENT"); //show the data sent
    debug.print(json); //show the data sent
    if(GPS.fix ==1){
       waitTime = 15000;
    }
    else{
       waitTime = 30000;
    }
    previous = millis();
    do{
       readGPS();
    }while(millis()-previous<waitTime);
    debug.listen();

    strcpy_P(buffer, (char*)pgm_read_word(&(CIPCLOSE))); 
    sendATcommand(buffer,"CLOSE OK",1000);
}
void connectWebServer(void){
        strcpy_P(buffer, (char*)pgm_read_word(&(CIPSTART))); 
        sendATcommand(buffer,"CONNECT OK",5000); //open connection to web server
        delay(delayBetweenCmds);

}
void reconnect(void){
            strcpy_P(buffer, (char*)pgm_read_word(&(CIPSHUT))); 
            sendATcommand(buffer, "OK", 1000); //reset ip connection if any are open
            delay(delayBetweenCmds);

            strcpy_P(buffer, (char*)pgm_read_word(&(CIPMUX))); 
            sendATcommand(buffer, "OK", 1000); //single connection
            delay(delayBetweenCmds);
            //Only when multi IP connection and GPRS application are both shut
            //down, AT+CIPMUX=0 is effective.
            //insert if here if reg then post data

            //ERROR IF NETWORK DISCONNECTED
            strcpy_P(buffer, (char*)pgm_read_word(&(ISCGATT))); 
            sendATcommand(buffer, "+CGATT=1", 1000); //check if connected +CGATT=1 means connected
            delay(delayBetweenCmds);

            strcpy_P(buffer, (char*)pgm_read_word(&(CGATT))); 
            sendATcommand(buffer, "OK", 1000); //attact to GPRS IF ERROR MEANS NO NETWORK
            delay(delayBetweenCmds);

            strcpy_P(buffer, (char*)pgm_read_word(&(CSTT))); 
            sendATcommand(buffer,"OK",1000); //connection data
            delay(delayBetweenCmds);
            
            strcpy_P(buffer, (char*)pgm_read_word(&(CIICR))); 
            sendATcommand(buffer,"OK",10000); //if reply is +PDP DEACT, MEANS OUT OF COVERAGE AREA
            delay(delayBetweenCmds);

            strcpy_P(buffer, (char*)pgm_read_word(&(CIFSR))); 
            sendATcommand(buffer,"OK",4000); //returns ip of the DeviceId
            delay(delayBetweenCmds);
        }
void createJSON(void){
  json = "{\"DeviceId\":";
  json += DeviceId;
  json +=",\"Longitude\":";
  json += String(GPS.longitudeDegrees,6);
  json += ",\"Latitude\":";
  json += String(GPS.latitudeDegrees,6);
  json += ",\"speed\":";
  json += String(GPS.speed,2);
  json += ",\"DateStamp\":\"";
  json += GPS.month;
  json += dash;
  json += GPS.day;
  json += dash;
  json += GPS.year;
  json += " ";
  json += GPS.hour;
  json += colon;
  json += GPS.minute;
  json += colon;
  json += GPS.seconds;
  json += "\",\"GPSfix\":";
  json += GPS.fix;
  json += ",\"FixQuality\":";
  json += GPS.fixquality;
  json += ",\"Satellites\":";
  json += GPS.satellites;
  //add gsm bars
  json += ",\"APIKey\":\"";
  strcpy_P(buffer, (char*)pgm_read_word(&(APIKey))); 
  json += buffer;
  json += "\"}";
}
//{"DeviceId":1,"Longitude":0.000000,"Latitude":0.000000,"speed":0,"DateStamp":"2015-8-10","GPSfix":0,"GPS_SignalQuality":13,"Satellites":3,"GSM_signal":10,"APIKey":"AIzaSyB4NyJ3xdQbHcj2v0C2CHtmgAPxSXYqmn0"}

int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int timeout){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;
    
    delay(100);
    while( Serial.available() > 0) Serial.read();    // Clean the input buffer
    Serial.flush();
    if (ATcommand[0] != '\0')
    {
    Serial.println(ATcommand);    // Send the AT command 
    }

    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
    if(Serial.available() != 0){    // if there are data in the UART input buffer, reads it and checks for the asnwer
        response[x] = Serial.read();
        if(debugging){
            debug.print(response[x]);
        }
        x++;
        if (strstr(response, expected_answer) != NULL)    // check if the desired answer (OK) is in the response of the module
        {
            answer = 1;
        }
        else if
        (strstr(response, "ERROR") != NULL)    // check if the desired answer (OK) is in the response of the module
        {
            answer = 0;
            break;
        }
    }
    }while((answer == 0) && ((millis() - previous) < timeout));    // Waits for the asnwer with time out

return answer;
}

/*
Flow
        at STARTUP                      STATUS:STATE: IP INITIAL
        AT+CIPSHUT                      STATUS:STATE: IP INITIAL
        AT+CIPMUX=0                     STATUS:STATE: IP INITIAL
        AT+CGATT?                       STATUS:STATE: IP INITIAL
        AT+CGATT=1                      STATUS:STATE: IP INITIAL
        AT+CSTT="TATA.DOCOMO.INTERNET"  STATUS:STATE: IP START
        AT+CIICR                        STATUS:STATE: IP GPRSACT
        AT+CIFSR                        STATUS:STATE: IP STATUS ready to rock and roll
        AT+CIPSTART="TCP","104.43.8.241","8084"   STATUS:STATE:CONNECT OK 
        AT+CIPSEND                      STATUS:
        SEND Post                       STATUS:
        READ response                   STATUS:
        AT+CIPCLOSE                     STATUS:
        CONNECTION CLOSED BY server     STATUS: TCP CLOSED
        LACK OF ACTIVITY                STATUS: TCP CLOSED
        pdp DEACT                       STATUS: PDP DEACT ,RECONNECT FROM START USE CIPSHUT

POST //Webservices.asmx/InsertTrackingData HTTP/1.1
Host: krtyabackoffice.cloudapp.net:8084
Content-Type: application/json
Content-Length: 140

{"DeviceId":1,"Longitude":0.000000,"Latitude":0.000000,"speed":0,"DateStamp":"2015-8-10","APIKey":"AIzaSyB4NyJ3xdQbHcj2v0C2CHtmgAPxSXYqmn0"}
{"DeviceId":1,"Longitude":0.000000,"Latitude":0.000000,"speed":0,"DateStamp":"2015-8-10","GPSfix":0,"GPS_SignalQuality":13,"Satellites":3,"GSM_signal":10,"APIKey":"AIzaSyB4NyJ3xdQbHcj2v0C2CHtmgAPxSXYqmn0"}
*/
/* Reply from server
{
  "d": [
    {
      "__type": "SchoolManagementSystem.Web.UI.AttendancesServices+TradingDeviceData",
      "status": "Succesfully",
      "Iserror": "False"
    }
  ]
}        
*/
